using System;
using Xunit;
using TDDconsole;

namespace TDDtest
{
    public class UnitTest1
    {
        [Fact]
        public void testAreaSquare()
        {
            Assert.Equal(81,Program.getAreaSquare(9));
        }

        [Fact]
        public void testAreaRectangle()
        {
            Assert.Equal(21,Program.getAreaRectangle(7,3));
        }

        [Fact]
        public void testAreaTriangle()
        {
            Assert.Equal(4,Program.getAreaTriangle(2,4));
        }
    }
}
